# Design Patterns

Run `pnpm i`

Then run code using `tsx` :
```
pnpm tsx design-patterns/value-object
pnpm tsx design-patterns/chain-of-responsibility
pnpm tsx design-patterns/specification
```