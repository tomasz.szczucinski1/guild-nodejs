type PaymentRequestT = {
  useNewBankApi: boolean;
  amount: number;
};

abstract class PaymentHandler {
  private nextHandler: PaymentHandler | null = null;

  setNextHandler(nextHandler: PaymentHandler): void {
    this.nextHandler = nextHandler;
  }

  public processPayment(request: PaymentRequestT): void {
    if (this.nextHandler) {
      this.nextHandler.processPayment(request);
    }
  }
}

class NewBankApiHandler extends PaymentHandler {
  public processPayment(request: PaymentRequestT): void {
    if (!request.useNewBankApi) return super.processPayment(request);
    console.log(`Processing payment with new bank API: $${request.amount}`);

    try {
      if (request.amount > 1000) throw new Error("Blah Blah");
    } catch (e) {
      console.error("Error caught in new API");
      return super.processPayment(request);
    }
  }
}

class OldBankApiHandler extends PaymentHandler {
  public processPayment(request: PaymentRequestT): void {
    console.log(`Processing payment with old bank API: $${request.amount}`);
  }
}

class PaymentProcessor {
  private paymentHandler: PaymentHandler;

  constructor() {
    const newBankApiHandler = new NewBankApiHandler();
    const oldBankApiHandler = new OldBankApiHandler();

    // Always fallback to old API, never other way
    newBankApiHandler.setNextHandler(oldBankApiHandler);

    this.paymentHandler = newBankApiHandler;
  }

  processPayment(request: PaymentRequestT): void {
    this.paymentHandler.processPayment(request);
  }
}

// Example usage
const paymentProcessor = new PaymentProcessor();

paymentProcessor.processPayment({ useNewBankApi: true, amount: 100 });

paymentProcessor.processPayment({ useNewBankApi: false, amount: 200 });

// This amount throws error in new api and fallback is going to old API
paymentProcessor.processPayment({ useNewBankApi: true, amount: 100000 });
