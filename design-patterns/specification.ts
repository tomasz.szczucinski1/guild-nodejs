type HeadersT = {
  [key: string]: string | number | boolean | undefined;
};

interface Specification<T> {
  isSatisfiedBy(item: T): boolean;
}

class HeaderSpecification implements Specification<HeadersT> {
  constructor(
    private headerName: string,
    private expectedValue: string | boolean | number,
  ) {}

  isSatisfiedBy(headers: HeadersT): boolean {
    const headerValue = headers[this.headerName];

    return this.expectedValue
      ? headerValue === this.expectedValue
      : headerValue !== undefined;
  }
}

const redirectPreferredSpec = new HeaderSpecification(
  "tpp-redirect-preferred",
  true,
);

console.log(
  redirectPreferredSpec.isSatisfiedBy({
    "tpp-redirect-preferred": false,
  }),
);

console.log(
  redirectPreferredSpec.isSatisfiedBy({
    "tpp-redirect-preferred": true,
  }),
);
