const paymentSad = (amount: string) => {
  const maxAmount = 100.0;
  const newAmount = parseFloat(amount);

  if (newAmount < maxAmount) {
    // ...
  }
};

class Amount {
  public readonly value: number;

  constructor(value: string | number) {
    // For example, but usually done in higher level (validation/transformation)
    this.value = typeof value === "string" ? parseFloat(value) : value;
  }

  public equals(other: Amount) {
    return this.value === other.value;
  }

  public lowerThan(other: Amount) {
    return this.value < other.value;
  }
}

const canPay = (amount: Amount, maxAmount: Amount) => {
  return amount.lowerThan(maxAmount);
};

const payment = (amount: Amount) => {
  // Do some stuff
  //
  const maxAmountLimit = new Amount(100.0);

  if (canPay(amount, maxAmountLimit)) {
    return { status: "completed" };
  } else {
    return { status: "rejected", message: "Max amount limit exceeded!" };
  }
};

const amount1 = new Amount("100.12");
const amount2 = new Amount(44.0);

console.log(amount1.value, payment(amount1));

console.log(amount2.value, payment(amount2));

// More real-world example of such approach in terms of payments
type Price = {
  currency: string; // It's string, but is it '$' or 'USD'
  amount: number;
};

const prices: Price[] = [
  {
    currency: "EUR",
    amount: 12.12,
  },
  {
    currency: "$",
    amount: 23.11,
  },
];

class Currency {
  /**
   * value: 3 letter symbol
   */
  constructor(public readonly value: string) {}

  // Some implementation
}

type _Price = {
  currency: Currency;
  amount: Amount;
};

const _prices: _Price[] = [
  {
    currency: new Currency("USD"),
    amount: new Amount("12.12"),
  },
];

console.log(
  "How it looks like when stringified and parse?",
  "\n",
  JSON.stringify(_prices, null, 2),
  "\n",
  JSON.parse(JSON.stringify(_prices)),
);
